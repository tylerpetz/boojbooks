<?php

namespace App\Models;

use App\Scopes\UserScope;
use App\Observers\UserIdObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Book extends Model
{
    use HasFactory;

    protected static function booted()
    {
        parent::booted();

        static::addGlobalScope(new UserScope);
        static::observe(UserIdObserver::class);
    }

    /**
     * Get the book's author
     */
    public function author() : BelongsTo
    {
        return $this->belongsTo(Author::class);
    }
}
