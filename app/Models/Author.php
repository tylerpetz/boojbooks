<?php

namespace App\Models;

use App\Observers\UserIdObserver;
use App\Scopes\UserScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected static function booted()
    {
        parent::booted();

        static::addGlobalScope(new UserScope);
        static::observe(UserIdObserver::class);
    }
}
