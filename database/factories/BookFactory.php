<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(2, 1),
            'description' => $this->faker->sentence(),
            'thumbnail' => $this->faker->imageUrl(),
            'user_id' => 1,
        ];
    }

    /**
     * Assign a random author id.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function randomAuthor($max_id)
    {
        return $this->state(function () use ($max_id) {
            return [
                'author_id' => $this->faker->numberBetween(1, $max_id),
            ];
        });
    }

    /**
     * Assign a random user id.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function randomUser($max_id)
    {
        return $this->state(function () use ($max_id) {
            return [
                'user_id' => $this->faker->numberBetween(1, $max_id),
            ];
        });
    }
}
